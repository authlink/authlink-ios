//
//  AccountTableViewCell.swift
//  authlinkios
//
//  Created by Christ, Jochen on 05.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var addedLabel: UILabel!
    
    var addedDate: Date? {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
            let dateString = dateFormatter.string(from: addedDate!)
            self.addedLabel.text = "This account was added at \(dateString)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
