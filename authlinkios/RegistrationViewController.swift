//
//  RegistrationViewController.swift
//  authlinkios
//
//  Created by Christ, Jochen on 24.08.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var identityField: UITextField!
    var verificationLink: String?
    var registrationId: String?

    @IBAction func sendVerificationToken(_ sender: Any) {
        guard let normalizedUser = getNormalizedUser() else {
            return
        }
    
        let body: Parameters = [
            "user": normalizedUser
        ]
        Alamofire.request("http://api.test.authlink.net:8080/registrations", method: .post, parameters: body, encoding: JSONEncoding.default)
            .responseJSON{response in
                debugPrint(response)
                switch response.result {
                case .success:
                    if let json = response.result.value as? [String: Any],
                        let registrationId = json["registrationId"] as? String,
                        let links = json["_links"] as? [String: Any],
                        let verificationLink = links["verification"] as? [String: String] {
                            self.registrationId = registrationId
                            self.verificationLink = verificationLink["href"]
                    }
                    self.performSegue(withIdentifier: "sendVerfication", sender: self)
                case .failure(let error):
                    print(error)
                    self.alert(message: error as! String, title: "Registration Failed")
                }
            }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "sendVerfication"){
            if let nextScene = segue.destination as? VerificationViewController,
                let verificationLink = self.verificationLink {
                nextScene.user = getNormalizedUser()
                nextScene.registrationId = self.registrationId
                nextScene.verificationLink = verificationLink
            }
        }
        
    }
    
    func getNormalizedUser() -> String? {
        return identityField.text?.trimmingCharacters(in: .whitespaces).lowercased()
    }
    
}
