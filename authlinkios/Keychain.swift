//
//  Keychain.swift
//  authlinkios
//
//  Created by Christ, Jochen on 24.08.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation

class Keychain {
    
    static func generateKeyPair(id: String)->Bool {
        let tag = "net.authlink.id." + id
        return generateKeyPair(publicTag: tag + ".public", privateTag: tag + ".private")
    }
    
    
    static func generateKeyPair(publicTag: String, privateTag: String)->Bool {
        let keySize = 2048
        let privateAttributes = [String(kSecAttrIsPermanent): true,
                                 String(kSecAttrApplicationTag): privateTag] as [String : Any]
        let publicAttributes = [String(kSecAttrIsPermanent): true,
                                String(kSecAttrApplicationTag): publicTag] as [String : Any]
        
        let pairAttributes = [String(kSecAttrKeyType): kSecAttrKeyTypeRSA,
                              String(kSecAttrKeySizeInBits): keySize,
                              String(kSecPublicKeyAttrs): publicAttributes,
                              String(kSecPrivateKeyAttrs): privateAttributes] as [String : Any]
        
        var publicRef: SecKey?
        var privateRef: SecKey?
        switch SecKeyGeneratePair(pairAttributes as CFDictionary, &publicRef, &privateRef) {
            case noErr: return true
            default: return false
        }
    }
    
    static func obtainKey(_ tag: String) -> SecKey? {
        var keyRef: AnyObject?
        let query: Dictionary<String, AnyObject> = [
            String(kSecAttrKeyType): kSecAttrKeyTypeRSA,
            String(kSecReturnRef): kCFBooleanTrue as CFBoolean,
            String(kSecClass): kSecClassKey as CFString,
            String(kSecAttrApplicationTag): tag as CFString,
            ]
        
        let status = SecItemCopyMatching(query as CFDictionary, &keyRef)
        
        switch status {
        case noErr:
            if let ref = keyRef {
                return (ref as! SecKey)
            }
        default:
            break
        }
        
        return nil
    }

    
    static func obtainKeyData(_ tag: String) -> Data? {
        var keyRef: AnyObject?
        let query: Dictionary<String, AnyObject> = [
            String(kSecAttrKeyType): kSecAttrKeyTypeRSA,
            String(kSecReturnData): kCFBooleanTrue as CFBoolean,
            String(kSecClass): kSecClassKey as CFString,
            String(kSecAttrApplicationTag): tag as CFString,
            ]
        
        let result: Data?
        
        switch SecItemCopyMatching(query as CFDictionary, &keyRef) {
        case noErr:
            result = keyRef as? Data
        default:
            result = nil
        }
        
        return result
    }

    
    static func deleteKey(_ tag: String) -> Bool {
        let query: Dictionary<String, AnyObject> = [
            String(kSecAttrKeyType): kSecAttrKeyTypeRSA,
            String(kSecClass): kSecClassKey as CFString,
            String(kSecAttrApplicationTag): tag as CFString]
        
        return SecItemDelete(query as CFDictionary) == noErr
    }

}
