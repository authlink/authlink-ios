//
//  AccountDetailsViewController.swift
//  authlinkios
//
//  Created by Christ, Jochen on 05.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import UIKit

class AccountDetailsViewController: UITableViewController {

    var account: Account?
    
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var publicKeyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let account = account {
            identifierLabel.text = account.user
            idLabel.text = account.id
            let keyTag = account.keyTag! + ".public"
            let keyData = Keychain.obtainKeyData(keyTag)
            if let keyData = keyData {
                publicKeyLabel.text = keyData.base64EncodedString()
                print("Keydata: " + keyData.base64EncodedString())
            } else {
               publicKeyLabel.text = "No Key for " + keyTag
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
