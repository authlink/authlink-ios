//
//  AuthenticationChallenge.swift
//  authlinkios
//
//  Created by Christ, Jochen on 07.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation

struct AuthenticationChallenge {

    var authenticationId: String
    var client: String
    var nonce: String
    var registrationId: String
    var timestamp: String
    var user: String
    var verificationLink: String
    
    func stringToSign() -> String {
        return "\(registrationId)|\(authenticationId)|\(client)|\(user)|\(timestamp)|\(nonce)"
    }
    
}
