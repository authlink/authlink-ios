//
//  AuthenticationHandler.swift
//  authlinkios
//
//  Created by Christ, Jochen on 07.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import SwiftyJSON

/*
 Singleton
 */
class AuthenticationHandler {
    
    static let sharedInstance = AuthenticationHandler()
    private init() {}
    
    let popup = AuthenticationPopup()
    
    // TODO sync and isPopupShown should be replaced with DispatchQueue
    var isPopupShown = false
    
    func handleAuthenticationChallenges() {
        synced(self) {
            self.getPendingAuthenticationChallenges{authenticationChallenge in
                self.showAuthenticationChallenge(challenge: authenticationChallenge)
            }
        }
    }
    
    func synced(_ lock: Any, closure: @escaping () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
    
    func showAuthenticationChallenge(challenge: AuthenticationChallenge) {
        
        if(self.isPopupShown) {
            print("Another authentication is currently shown")
            return
        }
        
        guard let account = getAccount(id: challenge.registrationId) else {
            print("Account \(challenge.registrationId) not found")
            return
        }
        
        let privateKey = Keychain.obtainKey(account.keyTag! + ".private")!
        
        self.isPopupShown = true
        popup.showPopup(client: challenge.client, user: account.user!) {
            let signature = Signature.sign(stringToSign: challenge.stringToSign(), privateKey: privateKey)
            print("Signature for authentication: \(signature)")

            print("Sending Signature to backend")
            let parameters: Parameters = ["registrationId": challenge.registrationId,
                                          "signature": signature]
            Alamofire.request(challenge.verificationLink, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .responseString {response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        print("OK")
                    case .failure(let error):
                        print(error)
                    }
                    self.isPopupShown = false
                    self.handleAuthenticationChallenges()
                }
            
        }
        
    }

    func getPendingAuthenticationChallenges(completion: @escaping(AuthenticationChallenge) -> Void) {
        
        for account in loadAccounts() {
            
            guard let user = account.user else {
                continue
            }
            
            let parameters: Parameters = ["user": user]
            Alamofire.request("http://api.test.authlink.net:8080/authentications", parameters: parameters)
                .responseJSON{response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        print("challenges from server \(response)")
                        var json = JSON(response.value!)
                        for (_, subJson) in json["_embedded"]["authenticationChallengeList"] {
                            
                            if let authenticationId = subJson["authenticationId"].string,
                            let client = subJson["client"].string,
                            let nonce = subJson["nonce"].string,
                            let timestamp = subJson["timestamp"].string,
                            let verificationLink = subJson["_links"]["verification"]["href"].string {
                            
                                let authenticationChallenge = AuthenticationChallenge(authenticationId: authenticationId, client: client, nonce: nonce, registrationId: account.id!, timestamp: timestamp, user: user, verificationLink: verificationLink)
                                print(authenticationChallenge)
                                completion(authenticationChallenge)
                            }
                        }
                    
                    case .failure(let error):
                        print(error)
                    }
            }
            
        }
    }
    
    
    func loadAccounts() -> [Account] {
        let request: NSFetchRequest<Account> = Account.fetchRequest()
        return try! AppDelegate.viewContext.fetch(request)
    }
    
    func getAccount(id: String) -> Account? {
        let request: NSFetchRequest<Account> = Account.fetchRequest()
        request.predicate = NSPredicate(format: "id == %@", id)
        let accounts = try! AppDelegate.viewContext.fetch(request)
        
        if accounts.count > 0 {
            return accounts[0]
        } else {
            return nil
        }
    }
    
}
