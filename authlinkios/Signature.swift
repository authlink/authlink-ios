//
//  Signature.swift
//  authlinkios
//
//  Created by Christ, Jochen on 07.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation

class Signature {
    
    static func sign(stringToSign: String, privateKey: SecKey) -> String {
        
        print("stringToSign: \(stringToSign)")
        
        let algorithm: SecKeyAlgorithm = .rsaSignatureMessagePKCS1v15SHA256
        guard SecKeyIsAlgorithmSupported(privateKey, .sign, algorithm) else {
            fatalError("key does not support algorithm")
        }
        
        var error: Unmanaged<CFError>?
        guard let signature = SecKeyCreateSignature(privateKey,
                                                    algorithm,
                                                    stringToSign.data(using: .utf8)! as CFData,
                                                    &error) as Data? else {
                                                        print(error?.takeRetainedValue() as Any)
             return ""
        }
        
        print("Signature: \(signature.base64EncodedData())")
        
        return signature.base64EncodedString()
        
    }
}
