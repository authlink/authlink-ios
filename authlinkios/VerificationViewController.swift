//
//  VerificationViewController.swift
//  authlinkios
//
//  Created by Christ, Jochen on 24.08.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class VerificationViewController : UIViewController {

    var user: String?

    var registrationId: String?
    var verificationLink: String?
    
    @IBOutlet weak var tokenField: UITextField!
    
    @IBAction func verify(_ sender: Any) {
        print("Veriy", user!)
        
        guard let challengeResponse = tokenField.text, challengeResponse != "" else {
            print("Challenge Response is empty")
            return
        }
        
        guard let registrationId = self.registrationId else {
            print("registrationId is not set")
            return
        }
        
        // accountId
        let tag = "net.authlink.id." + registrationId

        // create private and public key and save to keychain
        let keyGenerated = Keychain.generateKeyPair(id: registrationId)
        if(!keyGenerated) {
            alert(message: "Key Generation failed!")
        }
        
        // save account details on local device
        let context = AppDelegate.viewContext
        let account = Account(context: context)
            account.id = registrationId
            account.user = user!
            account.keyTag = tag
            account.created = Date() as NSDate
        
        
        do {
            try context.save()
        } catch {
            print("Error while saving context", error)
            return
        }
        
        // send public key to server (with the token)
        if let data = Keychain.obtainKeyData(account.keyTag! + ".public") {
            
            let exportImportManager = CryptoExportImportManager()
            if let exportableKey = exportImportManager.exportPublicKeyToDER(data, keyType: kSecAttrKeyTypeRSA as String, keySize: 2048)?.base64EncodedString() {
                
                print("exportableKey: ", exportableKey)
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let deviceToken = appDelegate.deviceToken
                
                let body: Parameters = [
                    "challengeResponse": challengeResponse,
                    "deviceToken": deviceToken as Any,
                    "publicKey": exportableKey
                ]
                
                
                Alamofire.request(verificationLink!, method: .post, parameters: body, encoding: JSONEncoding.default)
                    .responseString{response in
                        debugPrint(response)
                        switch response.result {
                        case .success:
                            print("Successfully verified registration")
                            self.performSegue(withIdentifier: "goBack", sender: self)
                        case .failure(let error):
                            // TODO handle wrong token
                            print(error)
                            
                        }
                }
            } else {
                print("Could not convert to PEM")
            }

        }
            
    }
    


    
}
