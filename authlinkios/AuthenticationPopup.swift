//
//  Authorization.swift
//  authlinkios
//
//  Created by Christ, Jochen on 07.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation
import LocalAuthentication
import UIKit

class AuthenticationPopup {
    
    func showPopup(client: String, user: String, onSuccess: @escaping () -> Void) {
        let reason = "Confirm to sign in to \(client) as \(user)"
        
        let context = LAContext()
        var authError: NSError? = nil

        let useBiometrics = false
        
        if(useBiometrics) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authError) {
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason, reply: { (success, error) in
                    if success {
                        DispatchQueue.main.async {
                            onSuccess()
                        }
                    } else {
                        DispatchQueue.main.async {
                            //Authentication failed. Show alert indicating what error occurred
                            self.displayErrorMessage(error: error as! LAError )
                        }
                        
                    }
                })
            } else {
                // Touch ID is not available on this device
                self.showAlert(title: "Error", message: (authError?.localizedDescription)!)
            }
        } else {
            showSimpleConfirmationDialog(message: reason, onSuccess: onSuccess)
        }
    }
        
    
    func displayErrorMessage(error:LAError) {
        var message = ""
        switch error.code {
        case LAError.authenticationFailed:
            message = "Authentication was not successful because the user failed to provide valid credentials."
            break
        case LAError.userCancel:
            message = "Authentication was canceled by the user"
            break
        case LAError.userFallback:
            message = "Authentication was canceled because the user tapped the fallback button"
            break
        case LAError.touchIDNotEnrolled:
            message = "Authentication could not start because Touch ID has no enrolled fingers."
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device."
            break
        case LAError.systemCancel:
            message = "Authentication was canceled by system"
            break
        default:
            message = error.localizedDescription
        }
        
        self.showAlert(title: "Authentication Failed", message: message)
    }
    
    func showSimpleConfirmationDialog(message:String, onSuccess: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Confirm Login", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {(action: UIAlertAction!) in
            print ("Approved")
            onSuccess()
        }))
        alertController.addAction(UIAlertAction(title: "Deny", style: .cancel, handler: {(action: UIAlertAction!) in
            print ("Denied")
            self.showAlert(title: "Login denied", message: "You denied the login to the service")
        }))
        topViewController()?.present(alertController, animated: true, completion: nil)
    }
        
    func showAlert(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(actionButton)
        topViewController()?.present(alertController, animated: true, completion: nil)
    
    }
    
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

