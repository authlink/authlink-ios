//
//  AccountsViewController.swift
//  authlinkios
//
//  Created by Christ, Jochen on 05.09.17.
//  Copyright © 2017 authlink. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Alamofire

class AccountsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    var accounts = [Account]()
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        let cellIdentifier = "AccountTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AccountTableViewCell else {
            fatalError("The cell is not an instance of AccountTableViewCell")
        }
        let account = accounts[indexPath.row]
        cell.accountLabel.text = account.user
        cell.addedDate = account.created as Date?
        
        return cell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    // Delete Account
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            AppDelegate.viewContext.delete(accounts[indexPath.row])
            
            do {
                try AppDelegate.viewContext.save()
            } catch {
                print("Error while saving context", error)
            }
            
            loadAccounts()
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("performSegue")
        performSegue(withIdentifier: "showAccountDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare for segue \(segue)")
        if let viewController = segue.destination as? AccountDetailsViewController {
            viewController.account = accounts[tableView.indexPathForSelectedRow!.row]
        }
    }
    
    
    override func viewDidLoad() {
        // register table view
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        // pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        // top right add button
        let addButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add,  target: self, action: #selector(AccountsViewController.navigateToRegistration))
        self.navigationItem.rightBarButtonItem = addButton
        
        loadAccounts()
        guard accounts.count > 0 else {
            navigateToRegistration()
            return
        }

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        loadAccounts()
        tableView.reloadData()
        refresh(self)
    }
    
    func loadAccounts() {
        print("load accounts")
        let request: NSFetchRequest<Account> = Account.fetchRequest()
        let fetchedAccounts = try? AppDelegate.viewContext.fetch(request)
        
        guard let accounts = fetchedAccounts else {
            return
        }
        self.accounts = accounts
    }
    
    
    @objc func navigateToRegistration() {
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let registrationViewController = storyboard.instantiateViewController(withIdentifier: "RegistrationViewController")
        navigationController?.pushViewController(registrationViewController, animated: false)
        
    }
    
    @IBAction func goBack(segue: UIStoryboardSegue) {
    }
    
    
    @objc func refresh(_ sender:AnyObject) {
        print("Refresh")
        refreshControl.endRefreshing()

        AuthenticationHandler.sharedInstance.handleAuthenticationChallenges()
    }
}
